module FrontDoors

import SearchLight: AbstractModel, DbId
import Base: @kwdef

export FrontDoor

@kwdef mutable struct FrontDoor <: AbstractModel
  id::DbId = DbId()
end

end
