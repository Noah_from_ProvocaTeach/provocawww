module FrontDoorsController
using Genie.Renderer.Html

export front_door

function front_door()
  html(:frontdoors, :front_door_view, layout = :front_door_layout)
end

end
