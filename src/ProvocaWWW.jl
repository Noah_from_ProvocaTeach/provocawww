module ProvocaWWW

using Genie, Logging, LoggingExtras

function main()
  Base.eval(Main, :(const UserApp = ProvocaWWW))

  Genie.genie(; context = @__MODULE__)

  Base.eval(Main, :(const Genie = ProvocaWWW.Genie))
  Base.eval(Main, :(using Genie))
end

end
